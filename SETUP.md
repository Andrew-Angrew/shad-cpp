# Инструкция по настройке окружения и сдаче заданий

## Настройка окружения

[Читайте на вики](https://wiki.school.yandex.ru/shad/groups/2018/semester2/Cpp2/#instrukcijaponastrojjkeokruzhenija)

## Структура репозитория, система сборки

Код относящийся к отдельной задаче находится в отдельной директории
(`hello-world` и т.д.). Там же находится условие задачи
(`hello-world/README.md`).

Для сборки мы используем CMake, вам необходимо научиться им пользоваться.
Tutorial на официальном сайте написан чужими для хищников и учит вещам за
которые руки отрывают в нормальных командах, не советуем тратить на него время.
Лучше прочитайте [вот](http://derekmolloy.ie/hello-world-introductions-to-cmake/).

В каждой задаче обычно есть следующие файлы:

* `CMakeLists.txt` --- cmake-файл для сборки задачи.
* `test_*.cpp` --- сами тесты для задачи.
* `bench_*.cpp` --- бенчмарки (описание далее).
* `*.h, *.cpp` --- некоторые файлы, в которых вы должны реализовать то, что требуется в условии задачи.

Вам разрешено изменять только файлы, содержащие решение задачи, в противном случае система не примет вашу посылку.
Когда вы делаете посылку, система запускает `test.cpp`, а также бенчмарки, и в случае успешного завершения
задача вам засчитывается.

Обратите внимание, что в некоторых задачах `test.cpp` может либо вообще не содержать тестов, либо содержать не все тесты, которые будут на сервере --- об этом всегда сообщается в условии задачи.
В этом случае вы сами должны написать тесты и протестировать всю реализованную функциональность.

### Сборка

Для новичков мы рекомендуем использовать CLion. Сборка с его использованием будет подробно разобрана на 1 семинаре. Здесь
приведены инструкции для использования из консоли.

#### Linux & OSX

Создадим директорию со сборкой:
```
# В директории shad-cpp
mkdir build
cd build
```

Запустим cmake:

 - На маке
```
env CC=gcc-8 CXX=g++-8 cmake ..
```

 - На линуксе
```
cmake ..
```

При этом будет выведена некоторая служебная информация, в частности используемый компилятор. Для корректной сборки необходим `g++` версии не ниже 7.1 или `clang++` версии не ниже 5.0.

Наконец, соберем задачу `is-prime`:
```
make test_isprime
```

Все задачи включены в один проект, поэтому если вы попытаетесь собрать проект целиком, могут возникнуть ошибки. Поэтому при сборке нужно указывать конкретный исполняемый файл, который вам нужен.

Теперь в `build` есть исполняемый файл `test_isprime`, который и нужно запустить.

Обратите внимание, что помимо этого на сервере также осуществляется тестирование с включенными санитайзерами, которые проверяют наличие неопределенного поведения и некорректной работы с памятью
в программе. Осуществить такую сборку просто:
```
mkdir asan_build
cd asan_build
cmake -DCMAKE_BUILD_TYPE=ASAN ..
make test_isprime
```

Для ThreadSanitizera аналогично используйте сборку TSAN.

#### Windows

Если вы используете mingw и cygwin, то все шаги такие же, разве что asan сборку вы сделать не сможете. Чтобы работать из Visual Studio 2017, сделайте следующее:

1. Выберите Open Folder и откройте папку с задачей.
2. Зайдите в Cmake/Change Cmake Settings.
3. В тех конфигурациях, которые вам нужны, поменяйте содержимое buildRoot на "${workspaceRoot}\\\\build".
4. Теперь вы можете использовать Cmake/Build all для сборки. Запускать из Visual Studio (без дебага) неудобно, это лучше делать из терминала. В директории с задачей после
сборки должна появиться папка build, где и находится исполняемый файл.

Обратите внимание, что компилятор MSVC может давать иные предупреждения/ошибки компиляции, чем те, которые вы можете получить на сервере, где используется gcc.

## Тесты и бенчмарки

Обычно в задачах есть юниттесты и бенчмарки, часто также присутствует
и какое-то базовое решение задачи, которое может проходить тесты, а
может и нет.  В любом случае ваше решение должно проходить
тесты. Тесты на сервере могут немного отличаться.

На бенчмарки могут накладываться ограничения по времени и памяти, в
которые базовое решение обычно не будет укладываться. Ваше решение должно укладываться в эти ограничения, бенчмарки запускаются только в
Release сборке.

Для сборки бенчмарка используйте

```
make bench_isprime
```