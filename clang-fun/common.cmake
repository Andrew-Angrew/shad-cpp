include_directories(.)

set(CXX_STANDARD 17)
if (CLANG_VERSION_STRING VERSION_LESS 5.0)
  set(CXX_STANDARD 14)
endif()

set(CMAKE_CXX_FLAGS
    "${CMAKE_CXX_FLAGS} -std=c++${CXX_STANDARD} -Wno-unused-parameter -Wno-unused-variable -g -fno-rtti")

set(CMAKE_CXX_FLAGS_ASAN "-g -fsanitize=address,undefined -fno-sanitize-recover=all"
    CACHE STRING "Compiler flags in asan build"
    FORCE)
