#!/bin/bash

set -e

S=$(pwd)
B=$S/build/release

# ./$1/check-names $2/no-dict/*.cpp $2/../check_names.cpp -p $1 >$2/no-dict/result.txt
# ./$1/check-names $2/dict/*.cpp -p $1 -dict $2/dict/dict.txt >$2/dict/result.txt

$B/check-names $S/tests/no-dict/*.cpp $S/check_names.cpp -p $B >/tmp/no-dict-result.txt
$B/check-names $S/tests/dict/*.cpp -p $B -dict $S/tests/dict/dict.txt >/tmp/dict-result.txt

diff $S/tests/no-dict/result.txt /tmp/no-dict-result.txt
diff $S/tests/dict/result.txt /tmp/dict-result.txt
