cmake_minimum_required(VERSION 3.5)
project(executors)

if (TEST_SOLUTION)
  include_directories(../private/executors)
  set(SRCS ../private/executors/executors.cpp)
else()
  include_directories(executors)
  set(SRCS executors/executors.cpp) # Add new source files here
endif()

include(../common.cmake)

add_library(executors ${SRCS})

add_gtest(test_executors
  test_executors.cpp
  test_future.cpp)
target_link_libraries(test_executors executors)

add_benchmark(bench_executors run.cpp)
target_link_libraries(bench_executors executors)
