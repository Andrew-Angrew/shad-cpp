cmake_minimum_required(VERSION 2.8)
project(micro-arch)

include(../common.cmake)

add_benchmark(bench_micro_arch bench.cpp)
add_benchmark(bench_search bench_search.cpp)
add_benchmark(bench_heap bench_heap.cpp)
add_benchmark(bench_float bench_float.cpp)
add_benchmark(bench_scalar bench_scalar.cpp)
add_benchmark(bench_matrix bench_matrix.cpp)
