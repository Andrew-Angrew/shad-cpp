#include <benchmark/benchmark.h>
#include <array>

const std::array<double, 4> values{1e-10, 1e-100, 1e-300, 1e-305};

void run(benchmark::State& state) {
    double sum = 0.0;
    double value = values[state.range(0)];
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(sum += value / 10000.0);
    }
}

BENCHMARK(run)->DenseRange(0, values.size() - 1, 1);

BENCHMARK_MAIN();
