#include <catch.hpp>

#include <chrono>
#include <vector>
#include <thread>

#include <spin_lock.h>


TEST_CASE("Correctness") {
    SpinLock spin;
    std::thread first([&spin]() {
        spin.lock();
        std::this_thread::sleep_for(std::chrono::milliseconds(1500));
        spin.unlock();
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    std::thread second([&spin]() {
        auto start = std::chrono::high_resolution_clock::now();
        spin.lock();
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = (std::chrono::duration_cast<std::chrono::seconds>(end - start)).count();
        REQUIRE(0.9 <= duration);
        REQUIRE(duration <= 1.1);
        spin.unlock();
    });
    first.join();
    second.join();
}


TEST_CASE("Concurrency") {
    int threads_count = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    threads.reserve(threads_count);
    int counter = 0;
    SpinLock spin;
    for (int i = 0; i < threads_count; ++i)
        threads.emplace_back([&counter, &spin]() {
            for (int j = 0; j < 100; ++j) {
                spin.lock();
                ++counter;
                spin.unlock();
            }
        });

    for (auto& thread : threads) {
        thread.join();
    }

    REQUIRE(100 * threads_count == counter);
}
